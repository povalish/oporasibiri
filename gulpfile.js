var gulp 					= require('gulp'),
		browserSync		= require('browser-sync').create();



gulp.task('server', function() {
	browserSync.init({
		server: './build/'
	});

	gulp.watch('./build/**/*.html').on('change', browserSync.reload);
	gulp.watch('./build/**/*.css').on('change', browserSync.reload);
	gulp.watch('./build/**/*.js').on('change', browserSync.reload);
});