<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Опора сибири</title>

    <!-- Bootstrap -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/media.css" rel="stylesheet">

    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/ico/favicon.png" type="image/png" />
    <script src='https://www.google.com/recaptcha/api.js'></script>
  </head>


  <body>
<div class="topbg">
  <div class="container">
    <header>
      <div class="logo">
        <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="">
      </div>
      <div class="header__subtitle">
        ОПОРЫ УЛИЧНОГО ОСВЕЩЕНИЯ. ПРОЖЕКТОРНЫЕ МАЧТЫ. СВЕТОДИОДНОЕ ОСВЕЩЕНИЕ
      </div>
      <nav>
        <li><a href="/">ГЛАВНАЯ</a></li>
        <li><a href="/catalog/">КАТАЛОГ</a></li>
        <li><a href="/nashi-raboty/">ОБЪЕКТЫ</a></li>
        <li><a href="/fotogalereya2016/">ФОТОГАЛЕРЕЯ</a></li>
        <li><a href="/kontakt/">КОНТАКТЫ</a></li>
      </nav>
      <div class="phones">
        <div class="phones__1">+7(391) 214-93-83</div>
        <div class="phones__2">+7(391) 214-92-22</div>
        <div>
          <span>Красноярск, ул. Брянская 360/2</span>
        </div>
        <!-- <span>+7(391)</span>214-93-93<br>
        <div class="hidden-xs"><span>+7(391)</span>214-92-22<br></div>
        <span>Красноярск, ул. Караульная 88</span> -->
      </div>
      <div class="maintext">
        <div class="maintext__title">
          <h1>Производство металлических опор и светотехники в Сибири</h1>
        </div>
        <div class="maintext__menu">
          Разработка
          <span>
            <img src="img/arrow.png" alt="">
          </span>
          Изготовление
          <span>
            <img src="img/arrow.png" alt="">
          </span>
          Монтаж
        </div>
      </div>
    </header>
  </div>
  </div>
<div id="content">
    <section>
      <div class="sectionHeader">
        <span>Почему с нами нужно работать?</span>
      </div>
      <div class="container">
        <div class="sectionBlock first">
          <b>Это выгодно</b>
          <div class="informItems col-lg-offset-4">
            <div class="infoItem firstItem" style="margin-bottom: 6px;">
              <span>Грарантировано низкие цены</span>
              Собвственное производство опор и светотехники
            </div>
             <div class="infoItem secondItem">
                <span>Оплата по факту получения продукции </span>
                Подробности уточняйте у менеджеров компании
             </div>
             <div class="infoItem thirdItem">
                <span>Выставим счет за 1 час</span>
                При звонке в офис компании для подачи заявки
             </div>
             <div class="infoItem firstItem">
                <span>Широкий ассортимент продукции</span>
                Более 100 конструкций опор и мачт и уникальных наименований
             </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div class="container">
        <div class="sectionBlock second">
          <b>Это надежно</b>
            <div class="informItems col-lg-offset-1">
            <div class="SBinfoItem SBfirstItem">
              <span>Больше 1000 произведенных опор и мачт</span>
            </div>
            <div class="SBinfoItem SBsecondItem" style="margin-bottom: 10px;">
              <span>Более 20 готовых объектов в г. Красноярск</span>
              Которые вы можете посетить лично и убедиться в качестве продукции
            </div>
            <div class="SBinfoItem SBthirdItem">
              <span>Отлаженная схема доставки на Дальний Восток</span>
              Приморский край 5-7 дней, Южно-Сахалинск от 10 дней
            </div>
            <div class="SBinfoItem SBfourthItem">
              <span>Гарантия на опоры до 20 лет</span>
            </div>
        </div>
      </div>
    </section>
    <section style="height:600px;" class="catalog_">
      <div class="sectionHeader">
        <span>Скачайте каталог</span>
      </div>
      <div class="floor2" data-stellar-background-ratio="0.3">
      <div class="prevsite" style="background: url(<?php echo get_template_directory_uri(); ?>/img/floor2bg.jpg) no-repeat;margin-top: 0;" data-stellar-background-ratio="0.3"></div>
      <div class="container">
        <div class="col-lg-6">
          <div class="katalog">
            <a href="<?php echo get_template_directory_uri(); ?>/catalog/catalog2016.pdf" download></a>
          </div>
        </div>
      </div>
      </div>
    </section>
    <section class="floor3">
        <div class="sectionHeader">
          <span>Почему выбирают металлические опоры?</span>
        </div>

        <div class="container">
          <div class="diagrams">
            <img src="img/diagrams.png" alt="">
          </div>

          <div class="diagram__descr">
            <div class="descr__main">
              У каждого вида опор есть свои достоинства и недостатки
            </div>
            <div class="descr__sub">
              Узнайте какие приемущества можно получить приобретая нашу продукцию
            </div>
          </div>

          <div class="diagram__button">
            <button>Подробнее</button>
          </div>
        </div>
    </section>
    <section>
      <div class="container">
        <div class="sectionBlock floor5">
          <b>Опоры освещения - это красиво!</b>
            <div class="informItems col-lg-offset-1">
            <div class="SBinfoItem">
              <span>Современные решения</span>
              Новейшие технологии производства новый уровень качества продукции
            </div>
            <div class="SBinfoItem" style="margin-bottom: 10px;">
              <span>Классическая изящность</span>
              Формы и линии, которые не устаревают
            </div>
            <div class="SBinfoItem">
              <span>Безупречный внешний вид</span>
              Качественное покрытие остается неизменным долгие годы
            </div>
            <div class="SBinfoItem">
              <span>Привлекательная простота</span>
              Красиво - не значит сложно
            </div>
        </div>
      </div>
    </section>
    <section style="padding: 100px" class="floor6 hidden-xs">
        <b>Выбери на свой вкус</b>
        <div class="lampsSecond">
          <span class="Lamps Lamp1"></span>
          <span class="Lamps Lamp2"></span>
          <span class="Lamps Lamp3"></span>
          <span class="Lamps Lamp4"></span>
          <span class="Lamps Lamp5"></span>
          <span class="Lamps Lamp6"></span>
          <span class="Lamps Lamp7"></span>
        </div>
        <div class="diagram__button">
          <button>Узнать больше</button>
        </div>
    </section>
    <section class="floor7sec">
      <div class="floor7" data-stellar-background-ratio="0.3">
      <div class="sectionHeader">
        <span>Какую светотенику использовать?</span>
      </div>
       <svg width="0" height="0">
          <clipPath id="clipPolygon">
            <polygon points="93 52,186 0,0 0">
            </polygon>
          </clipPath>
        </svg>
      <div class="container">
        <img class="hidden-xs lightbox" src="<?php echo get_template_directory_uri(); ?>/img/lampfloor7.png" alt="">
        <div class="shape1">
          <img src="img/shapes2.png" alt="">
          <div class="shape__text1">
            <div class="text__header1">Светильники ИСКРА</div>
            <div class="text__body">Для освещение улиц, автодорог, парковок, прилегающих территорий</div>
            <div class="text__header2">От 60 до 210 Вт</div>
          </div>
        </div>
        <div class="shape2">
          <img src="img/shapes3.png" alt="">
          <div class="shape__text2">
            <div class="text__header1">Прожекторы ИМПУЛЬС</div>
            <div class="text__body">Для освещения больших площадей, ЖД узлов, авторазвязок</div>
            <div class="text__header2">От 120 до 2000Вт</div>
          </div>
        </div>
      </div>
      </div>
    </section>
    <div class="sectionHeader zindex5">
      <div class="container releative">
        <div class="monitor hidden-xs"></div>
          <ul class="right-aligned">
            <span>Светодиодные светильники и прожекторы</span>
            <li>Собственное производство</li>
            <li>Конфигурации под любую задачу</li>
            <li>Гарантия 5 лет</li>
          </ul>
          <div class="diagram__button center-btn">
            <button>Подробнее</button>
          </div>
      </div>
    </div>
    <section class="partners">
      <div class="container">
        <h3>Нашу продукцию приобретают</h3>
        <div class="sectionBlock partners__bg">
          <div class="partners-container">
            <div class="partner"><img src="<?php echo get_template_directory_uri(); ?>/img/trans.png"></div>
            <div class="partner"><img src="<?php echo get_template_directory_uri(); ?>/img/univers.png"></div>
            <div class="partner"><img src="<?php echo get_template_directory_uri(); ?>/img/gas.png"></div>
            <div class="partner"><img src="<?php echo get_template_directory_uri(); ?>/img/planet.png"></div>
            <div class="partner"><img src="<?php echo get_template_directory_uri(); ?>/img/rjd.png"></div>
            <div class="partner"><img src="<?php echo get_template_directory_uri(); ?>/img/ok.png"></div>
            <div class="partner"><img src="<?php echo get_template_directory_uri(); ?>/img/lenta.png"></div>
          </div>
        </div>
      </div>
    </section>
    <section class="floor6 hidden-xs">
      <h3>Производимая продукция</h3>
      <div class="lampssecond2">
        <a href="/category/catalog/opory-granenye/" class="Lamps secondLamp1">Опоры граненые</a>
        <a href="/category/catalog/opory-trubchatye/" class="Lamps secondLamp2">Опоры трубчатые</a>
        <a href="/category/catalog/machty-osveshheniya/" class="Lamps secondLamp3">Мачты прожекторные</a>
        <a href="/category/catalog/opory-parkovye/" class="Lamps secondLamp4">Опоры парковые</a>
        <a href="/category/catalog/machty-svyazi/" class="Lamps secondLamp5">Мачты связи</a>
        <a href="#" class="Lamps secondLamp6">Опоры складывающиеся</a>
        <a href="#" class="Lamps secondLamp7">Опоры переносного типа</a>
      </div>
    </section>
<section class="feedback">
  <div class="container">
    <form class="col-lg-offset-3 col-lg-6" action="" method="post">
      <?php echo do_shortcode( '[contact-form-7 id="5" title="Свяжитесь с нами"]' ); ?>
      <div class="g-recaptcha" data-sitekey="6Lfm-yYTAAAAAETl00K5QYDCDDyFXrfv7Xhmc3dP"></div>
    </form>
  </div>
</section>
<?php get_footer(); ?>