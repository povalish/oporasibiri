<?php get_header(); ?>
<div class="container">
	<div class="col-lg-offset-1 col-lg-10">
		<?php if (have_posts() ) : query_posts ('p=10');
			while (have_posts()) : the_post(); ?>
			
			<div class="postinfo">
				<h3><?php the_title(); ?></h3>
				<?php the_post_thumbnail(array(160)); ?>
				<ul>
					<li><?php echo get_post_meta($post->ID, 'key_words', true); ?></li>
					<li>По вопросам установки уточняйте у специалистов компании</li>
				</ul>
			</div>
			<div class="postText">
				<?php the_content(); ?>
			</div>
			<? endwhile; endif; wp_reset_query(); ?>
	</div>
</div>
<?php get_footer(); ?>