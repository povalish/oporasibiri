<footer>
    <div class="container">
          <ul class="footerColumn1">
            <span>О КОМПАНИИ</span>
            <li><a href="/opory-osveshcheniya/">Объекты</a></li>
            <li><a href="/fotogalereya2016/">Фотогалерея</a></li>
            <li><a href="/kontakt/">Контакты</a></li>
          </ul>
          <ul class="footerColumn2">
            <span>КАТАЛОГ</span>
            <li><a href="/category/catalog/machty-osveshheniya/">Мачты освещения</a></li>
            <li><a href="/category/catalog/opory-granenye/">Опоры граненые конические</a></li>
            <li><a href="/category/catalog/opory-trubchatye/">Опоры трубчатые</a></li>
            <li><a href="/category/catalog/opory-parkovye/">Декоративное освещение</a></li>
            <li><a href="/category/catalog/machty-svyazi/">Мачты связи</a></li>
          </ul>
          <ul class="footerColumn3">
            <span>E-MAIL</span>
            <li><a href="#">oporasibiri@mail.ru</a></li>
          </ul>
          <ul class="footerColumn4">
            <span>ТЕЛЕФОН</span>
            <li class="footerContPhone"><a href="#">+7 (391) 214-93-93</a></li>
            <li class="footerContPhone"><a href="#">+7 (391) 214-92-22</a></li>
          </ul>


    </div>
<div class="metrika">
    <!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=36047960&amp;from=informer"
target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/36047960/3_0_071B33FF_071B33FF_1_pageviews"
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:36047960,lang:'ru'});return false}catch(e){}" /></a>
<!-- /Yandex.Metrika informer -->
</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter36047960 = new Ya.Metrika({
                    id:36047960,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/36047960" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-101741045-1', 'auto');
  ga('send', 'pageview');

</script>
</footer>
 <?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-2.0.2.js"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.stellar.min.js"></script>
    <script> $.stellar(); </script>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>


    <link rel="stylesheet" href="https://cdn.saas-support.com/widget/cbk.css">
<script type="text/javascript" src="https://cdn.saas-support.com/widget/cbk.js?wcb_code=1d14d11c3d34ee31292012d8fa556f16" charset="UTF-8" async></script>
 </body>
</html>
