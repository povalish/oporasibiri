<?php get_header(); ?>
   <div class="container"> 
    	<div class="pagehead">
      		<b><?php single_cat_title(); ?></b>
     	</div>
 	</div>
<?php
 	for($i=1; $i<=50; $i++){
 		$chek_cat = is_category($i);
 		if ($chek_cat == true) {
 			$cat_id = $i;
 			break;
 		}
 	}
?>
<div class="container">
	<?php if (have_posts() ) : query_posts ('cat='.$cat_id) ;
		while (have_posts()) : the_post(); ?>

		<div class="catalogItem col-lg-6">
			<?php the_post_thumbnail(array(100,100)); ?>
			<h3>
				<a href="<?php echo get_permalink(); ?>"> <?php the_title(); ?></a>
			</h3>
			<span><?php the_content(); ?></span>		
		</div>
			
			
	<? endwhile; endif; wp_reset_query(); ?> 
</div>

<?php get_footer(); ?>