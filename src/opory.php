<?php get_header(); ?>
<div class="container">
	<div class="col-lg-offset-1 col-lg-10">
	<?php $cat = get_the_category();?>
	<?php $post_id = get_the_ID(); ?>
		<?php if (have_posts() ) : query_posts ('p='.$post_id.'');
			while (have_posts()) : the_post(); ?>
			<h3><?php the_title(); ?></h3>
			<div class="postinfo">
				<?php the_post_thumbnail(array(230)); ?>

				<?php if ($cat[0]->cat_ID !== 9) { ?>
					<div class="morephoto">
						<?php show_thumbnails_list(); ?>
					</div>
				<?php } ?>

			</div>
			<div class="postText">
				<?php the_content(); ?>
			</div>
			<? endwhile; endif; wp_reset_query(); ?>
	</div>
</div>
<?php get_footer(); ?>