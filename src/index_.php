<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Опора сибири</title>

    <!-- Bootstrap -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/media.css" rel="stylesheet">

    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/ico/favicon.png" type="image/png" />
    <script src='https://www.google.com/recaptcha/api.js'></script>
  </head>
  

  <body>
<div class="topbg">
  <div class="container">
    <header>
      <div class="logo">
        <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="">
      </div>
      <nav>
        <li><a href="/catalog/">КАТАЛОГ</a></li>
        <li><a href="/nashi-raboty/">ОБЪЕКТЫ</a></li>
        <li><a href="/fotogalereya2016/">ФОТОГАЛЕРЕЯ</a></li>
        <li><a href="/kontakt/">КОНТАКТЫ</a></li>
      </nav>
      <div class="phones">
        <span>+7(391)</span>214-93-93<br>
        <div class="hidden-xs"><span>+7(391)</span>214-92-22<br></div>
        <span>Красноярск, ул. Караульная 88</span>
      </div>
      <div class="maintext">
        <h1>Производство металлических опор в Сибири</h1>
        Разработка <span>&#187;</span> Изготовление <span>&#187;</span> Монтаж
      </div>
    </header>
  </div>
  <a id="arrowdown" href="#content" class="arrowdown hidden-xs"></a>
  </div>
<div id="content">
<section>
  <div class="sectionHeader">
    <span>Почему с нами нужно работать?</span>
  </div>
  <div class="container">
    <div class="sectionBlock first">
      <b>Это выгодно</b>
      <div class="informItems col-lg-offset-4">
        <div class="infoItem firstItem" style="margin-bottom: 6px;">
          <span>С нами можно зарабатывать</span> 
          Приглашаем к сотрудничеству производителей и дистрибьюторов светотехнической продукции
        </div>
         <div class="infoItem secondItem">
            <span>Оплата по факту получения продукции </span> 
            Подробности уточняйте у менеджеров компании
         </div>
         <div class="infoItem thirdItem">
            <span>Выставим счет за 1 час</span> 
            При звонке в офис компании для подачи заявки
         </div> 
         <div class="infoItem firstItem">
            <span>Широкий ассортимент продукции</span> 
            Более 100 конструкций опор и мачт и уникальных наименований
         </div>                  
      </div>
    </div>
  </div>
</section>
<section>
  <div class="container">
    <div class="sectionBlock second">
      <b>Это надежно</b>
        <div class="informItems col-lg-offset-1">
        <div class="SBinfoItem SBfirstItem">
          <span>Больше 1000 произведенных опор и мачт</span> 
        </div>
        <div class="SBinfoItem SBsecondItem" style="margin-bottom: 10px;">
          <span>Более 20 готовых объектов в г. Красноярск</span>
          Которые вы можете посетить лично и убедиться в качестве продукции 
        </div>
        <div class="SBinfoItem SBthirdItem">
          <span>Отлаженная схема доставки на Дальний Восток</span>
          Приморский край 5-7 дней, Южно-Сахалинск от 10 дней
        </div>
        <div class="SBinfoItem SBfourthItem">
          <span>Гарантия на опоры до 20 лет</span>
        </div>        
    </div>
  </div>
</section>
<section style="height:600px;">
  <div class="floor2"data-stellar-background-ratio="0.3">
  <div class="prevsite" style="background: url(<?php echo get_template_directory_uri(); ?>/img/floor2bg.jpg) no-repeat;margin-top: 0;" data-stellar-background-ratio="0.3"></div>
  <div class="container">
    <div class="col-lg-6">
      <ul>
        <li>Тип покрытия</li>
        <li>Назначение</li>
        <li>Нормативные документы</li>
        <li>Допустимая нагрузка</li>
        <li>Условия эксплуатации</li>
        <li>Особенноси конструкции</li>
        <span>Всё это вы можете уточнить у специалистов компании или просмотрев наш каталог</span>
      </ul>
    </div>
    <div class="col-lg-6">
      <div class="katalog">
        <a href="<?php echo get_template_directory_uri(); ?>/catalog/catalog2016.pdf" download></a>
        Нажмите на картинку чтобы скачать наш каталог
      </div>
    </div>
  </div>
  </div>
</section>
<section class="floor3">
    <div class="sectionHeader">
      <span>Почему большенство выбирает металлические опоры?</span>
    </div>
    <div class="container"> 
    <div class="sectionBlock thirdFloor">
      <b>Сравнение и преимущество граненых опор</b>
      <div class="informItems col-lg-offset-4">
        <div class="characterItem">
            <span>Вес одной опоры</span>
            <img style="margin:10px 0 0 25px;" src="<?php echo get_template_directory_uri(); ?>/img/bubblesVes.png" alt="">
        </div>
        <div class="characterItem">
            <span>Максимальная нагрузка</span>
            <img style="margin: 10px 0 0 0;" src="<?php echo get_template_directory_uri(); ?>/img/bubblesNagr.png" alt="">
        </div>
        <div class="characterItem">
            <span>Срок службы</span>
            <img style="margin: 10px 0 0 20px;" src="<?php echo get_template_directory_uri(); ?>/img/bubblesNagr.png" alt="">
        </div> 
        <div class="characterItem">
            <span>Вместимость в одну машину</span>
            <img style="margin: 10px 0 0 20px;" src="<?php echo get_template_directory_uri(); ?>/img/bubblesVmestimost.png" alt="">
        </div>               
      </div>
    </div>
  </div> 
</section>
<section class="floor4">
    <div class="sectionHeader">
      <div class="container">
        <span class="tablMob">Таблицы размеров</span>
        <ul>
          <li>Опоры граненые ОГКф</li>
          <li>Опоры граненые ОГКп</li>
          <li>Опоры силовые ОГКСф</li>
          <li>Опоры силовые ОГКСп</li>
        </ul>
        <ul>
          <li>Опоры и мачты связи</li>
          <li>Опоры ОГКСкл складывающиеся</li>
          <li>Опоры трубчатые ОТф</li>
          <li>Опоры трубчатые ОТп</li>
        </ul>    
        <ul>
          <li>Мачты с мобильной короной ВМО</li>
          <li>Мачты со стационарной короной ВМС</li>
        </ul>             
      </div>
    </div>  
</section>
<section>
  <div class="container">
    <div class="sectionBlock floor5">
      <b>Опоры освещения - это красиво!</b>
        <div class="informItems col-lg-offset-1">
        <div class="SBinfoItem">
          <span>Современные решения</span> 
          Новейшие технологии производства новый уровень качества продукции
        </div>
        <div class="SBinfoItem" style="margin-bottom: 10px;">
          <span>Классическая изящность</span>
          Формы и линии, которые не устаревают
        </div>
        <div class="SBinfoItem">
          <span>Безупречный внешний вид</span>
          Качественное покрытие остается неизменным долгие годы
        </div>
        <div class="SBinfoItem">
          <span>Привлекательная простота</span>
          Красиво - не значит сложно
        </div>        
    </div>
  </div>
</section>
<section class="floor6 hidden-xs">
    <h3>От классики до Модерна. Выберите на свой вкус</h3>
    <div class="lampsSecond">
      <span class="Lamps Lamp1"></span>
      <span class="Lamps Lamp2"></span>
      <span class="Lamps Lamp3"></span>
      <span class="Lamps Lamp4"></span>
      <span class="Lamps Lamp5"></span>
      <span class="Lamps Lamp6"></span>
      <span class="Lamps Lamp7"></span>
    </div>
</section>
<section class="floor7sec">
  <div class="floor7" data-stellar-background-ratio="0.3">
  <div class="sectionHeader">
    <span>Поможем выбрать световое оборудование</span>
  </div>
  <div class="container">
    <ul>
      <li>
        Рекомендуемые источники света
        <div><span>Для опор ОГК</span><span>Для прожекторных мачт</span><span>Для парковых фонарей</span></div>
      </li>
      <li>
        Проконсультируем
        <div><span>Поможем выбрать оптимальное для вас решение</span></div>
      </li>
      <li>
        Рассчитаем
        <div><span>Произведем расчет необходимого уровня освещенности </span></div>
      </li>
      <li></li>
    </ul>
    <img class="hidden-xs" src="<?php echo get_template_directory_uri(); ?>/img/lampfloor7.png" alt="">
  </div>
  </div>
</section>
<div class="sectionHeader">
  <div class="container releative">
    <div class="monitor hidden-xs"></div>
      <ul>
        <span>Рекомендуемые источники света</span>
        <li>Для опор ОГК</li>
        <li>Для прожекторных мачт</li>
        <li>Для парковых фанарей</li>
      </ul>
  </div>
</div>
<section class="floor6 hidden-xs">
    <h3>Производимая продукция</h3>
    <div class="lampssecond2">
      <a href="/category/catalog/opory-granenye/" class="Lamps secondLamp1">Опоры граненые</a>
      <a href="/category/catalog/opory-trubchatye/" class="Lamps secondLamp2">Опоры трубчатые</a>
      <a href="/category/catalog/machty-osveshheniya/" class="Lamps secondLamp3">Мачты прожекторные</a>
      <a href="/category/catalog/opory-parkovye/" class="Lamps secondLamp4">Опоры парковые</a>
      <a href="/category/catalog/machty-svyazi/" class="Lamps secondLamp5">Мачты связи</a>
      <a href="#" class="Lamps secondLamp6">Опоры складывающиеся</a>
      <a href="#" class="Lamps secondLamp7">Опоры переносного типа</a>
    </div>
</section>
<section class="feedback">
  <div class="sectionHeader">
    <span>Свяжитесь с нами</span>
  </div>
  <div class="container">
    <form class="col-lg-offset-3 col-lg-6" action="" method="post">
      <?php echo do_shortcode( '[contact-form-7 id="5" title="Свяжитесь с нами"]' ); ?>
      <div class="g-recaptcha" data-sitekey="6Lfm-yYTAAAAAETl00K5QYDCDDyFXrfv7Xhmc3dP"></div>
    </form>
  </div>
</section>
<?php get_footer(); ?>