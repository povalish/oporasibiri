<?php get_header(); ?>
<div class="container">
    <div class="pagehead" style="margin-bottom:0;">
      <b>Фотогалерея</b>
     </div>
			<?php if (have_posts() ) : query_posts('p=20') ;
				while (have_posts()) : the_post(); ?>
				<div class="gallary col-lg-offset-1 col-lg-10">
					<?php show_thumbnails_list(); ?>
				</div>
			<? endwhile; endif; wp_reset_query(); ?>
</div>
<?php get_footer(); ?>