<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Опора сибири</title>
    <!-- Bootstrap -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/media.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/ico/favicon.png" type="image/png" />
 <?php wp_head(); ?>

  </head>
  <body>
 <div class="topbar">
 	<div class="container headCont">
 	  <div class="logoTopbar">
        <a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt=""></a>
        <nav class="topbarNav hidden-xs">
	        <li><a href="/catalog/">КАТАЛОГ</a></li>
	        <li><a href="/nashi-raboty/">ОБЪЕКТЫ</a></li>
	        <li><a href="/fotogalereya2016/">ФОТОГАЛЕРЕЯ</a></li>
	        <li><a href="/kontakt/">КОНТАКТЫ</a></li>
	    </nav>
      <a href="#menu" data-toggle="collapse" class="menu visible-xs"></a>
  <!--меню мобильное-->
      <div class="collapse" id="menu">
        <ul>
          <li><a href="/opory-osveshcheniya/">КАТАЛОГ</a></li>
          <li><a href="/nashi-raboty/">ОБЪЕКТЫ</a></li>
          <li><a href="/fotogalereya2016/">ФОТОГАЛЕРЕЯ</a></li>
          <li><a href="/kontakt/">КОНТАКТЫ</a></li>
        </ul>
      </div>
   <!--меню мобильное-->
	    <div class="phonesHead hidden-xs">
	        <span>+7(391)</span>214-93-93<br>
	        <span>+7(391)</span>214-92-22<br>
	        <span>Красноярск, ул. Караульная 88</span>
      </div>
      </div>
 	</div>
 </div>