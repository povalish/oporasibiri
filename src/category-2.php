<?php get_header(); ?>
   <div class="container"> 
    	<div class="pagehead">
      		<b>Каталог продукции</b>
     	</div>
<?php var_dump(the_category()); ?>
<?php
$args = array(
	'parent'                   => 2,
	'hide_empty'               => 0,
	'number'                   => '0',
	'taxonomy'                 => 'category',
	'pad_counts'               => true );
 
$catlist = get_categories($args);
foreach ($catlist as $categories_item) { ?>
	<div class="catalogItem col-lg-6">
		<?php if($imgcat1=get_field("imgcat1",$categories_item)){?>
			<img src="<?php echo $imgcat1;?>"/>
		<?php }?>
		<h3><a href="<?php echo get_category_link($categories_item); ?>">
		<?php echo $categories_item->cat_name ?></a></h3>
		<span><?php echo $categories_item->category_description; ?></span>		
	</div>
<?php } ?>

    </div>
<?php get_footer(); ?>